#!/usr/bin/env python3

import os
import sys
import shutil

if __name__ != "__main__":
    print("This script must be run as __main__")
    sys.exit(1)

if not shutil.which("xmake"):
    print("This script requires xmake to build Bamboo. Xmake can be found at https://xmake.io")
    sys.exit(1)

print("Building bamboo...")
os.system("xmake f -m release && xmake -P . && xmake run install")
print("Installing bamboo clibs and libs")
os.system("mkdir -p ~/.bamboo/libs/clibs/ && cp -r ./clibs/build/*.so ~/.bamboo/libs/clibs/ && cp -r ./libs/ ~/.bamboo/libs/")
print("Make sure you add `~/.bamboo/bin/` to your $PATH.")
