import os

# credit: https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
# yes, i could have done it on my own
# but i really didnt want too
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def getLOC():
    print(f"[{bcolors.OKGREEN}LOC{bcolors.ENDC}] starting...")
    loc = 0

    for dir in os.walk("src"):
        for fileName in dir[2]:
            with open(os.path.join(dir[0], fileName)) as file:
                linesInfile = 0
                for line in file:
                    line = line.rstrip(" \n\t\r\f")
                    if not line.startswith("//") and line != "":
                        loc += 1
                        linesInfile += 1
                print(f"[{bcolors.OKGREEN}LOC{bcolors.ENDC}] {os.path.join(dir[0], fileName)} : {linesInfile}")


    print(f"[{bcolors.OKGREEN}LOC{bcolors.ENDC}] Lines of code: {loc}")

getLOC()