import os

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def getSemis():
    print(f"[\033[92mSemicolons\033[0m] starting...")
    semis = 0

    for dir in os.walk("src"):
        for fileName in dir[2]:
            with open(os.path.join(dir[0], fileName)) as file:
                semisInFile = 0
                inMultilineComment = False
                inString = False
                for line in file:
                    line = line.strip("\n\r\f\t ")
                    i = 0
                    l = len(line)
                    while i < l:
                        char = line[i]
                        nextChar = line[i+1] if i < l - 1 else None
                        lastChar = line[i-1] if i > 0 else None

                        if char == "/" and nextChar == "/" and not inString:
                            break
                        elif char == "/" and nextChar == "*" and not inString:
                            inMultilineComment = True
                        elif char == "*" and nextChar == "/" and not inString:
                            inMultilineComment = False
                        elif char == ";" and not inMultilineComment and not inString:
                            semisInFile += 1
                            if fileName == "bamboo.cpp":
                                print(f"() {line}")
                        elif char == "\"" and lastChar != "\\":
                            inString = not inString

                        i += 1
                semis += semisInFile
                print(f"[\033[92mSemicolons\033[0m] {os.path.join(dir[0], fileName)} : {semisInFile}")

    print(f"[\033[92mSemicolons\033[0m] Semicolons: {semis}")

getSemis()
