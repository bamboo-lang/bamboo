import os

class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def getLOC():
	print(f"[{Colors.OKGREEN}LOC{Colors.ENDC}] starting...")
	total = 0

	for dir in os.walk("src"):
		for fileName in dir[2]:
			file = os.path.join(dir[0], fileName)
			size = os.path.getsize(file)
			print(f"[{Colors.OKGREEN}LOC{Colors.ENDC}] {file} : {Colors.OKGREEN}{size} bytes{Colors.ENDC} ({Colors.OKBLUE}{size/1000}KB{Colors.ENDC})")
			total += size

	print(f"[{Colors.OKGREEN}LOC{Colors.ENDC}] Total: {Colors.OKGREEN}{total} bytes{Colors.ENDC} ({Colors.OKBLUE}{total/1000}KB{Colors.ENDC})")

getLOC()