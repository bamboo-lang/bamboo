# Bamboo

Bamboo is a fast, stack-based, functional programming language.

```bamboo
"Hello, World!" #print
```

[Website](https://bamboo-lang.gitlab.io)

## Tutorial

See `/docs/tutorial-by-example.md`

## Installation

```sh
git clone https://gitlab.com/bamboo-lang/bamboo/
python3 install.py
# Replace with .zshrc for zsh or the equivalent for whatever shell you're using.
echo "export PATH=$PATH:$HOME/.bamboo/bin" > ~/.bashrc
```

## Dependancies

The only dependancy for Bamboo is `dl`, which should be installed by default on literally all Linux machines.

> `dl` is used for loading shared libraries

## Performance

Bamboo is extremely fast, which is in part due to the simplicity of the language, benchmarks can be found at the benchmark repo here: [/bamboo-lang/benchmarks](https://gitlab.com/bamboo-lang/benchmarks/)

Typically, the performance has been quite close to compiled C++ (within a single millisecond most the time), and **significantly** faster than Python.

Whilst Bamboo is being developed, performance is being kept in mind to ensure that Bamboo does not become overly slow.

## Xmake

Bamboo uses xmake for compilation. Pretty much, use `xmake` to build and `xmake run` to run the `example.bam` file.

[Xmake's website](https://xmake.io/)
