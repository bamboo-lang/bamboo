# Bamboolib API documentation

This document has information on, and refferences to all the functions and custom types in Bamboo. Documentation on Bamboo's builtin functions can be found in `doc/builtins.md`

Starting from the top, everything will be explained in this order, and format:

*The following list is the order of files covered*

- **types.hpp**
- **utils.hpp**
- **interpreter.hpp**
- **parser.hpp**

Each file will be presented in this format:

## file.hpp

<details>
<summary>Macros</summary>

```cpp
#define A 0
#define B 1
#define C 2
```

</details>

<details>
<summary>Types</summary>

```cpp
// Type description
typedef int a
struct B;
class<T> C;
```

</details>

#### `inline void foo(int a, int b)`

description

Paramters:

- `a` - Description
- `b` - Description

<details>
<summary>Overloads</summary>

```cpp
inline void foo(char a, char b)
inline void foo(int a, int b)
inline void foo(double a, double b)
```
</details>

#### `class Foo`

Description

```cpp
class Foo {
    // Member Description
    int a;
    // Member Description
    int b;
    // Member Description
    int c;
};
```

<details>
<summary>Methods</summary>

- `void A(int a, int b);`

    Description.

    Paramaters:
    - `a` - Description
    - `b` - Description

- `void B(int a, int b);`

    Description.

    Paramaters:
    - `a` - Description
    - `b` - Description
</details>

---
### types.hpp
---

<details>
<summary>Macros</summary>

```cpp
#define TOKEN_TYPE_STR 0
#define TOKEN_TYPE_INT 1
#define TOKEN_TYPE_SUB 2
#define HOLDS std::holds_alternative
```
</details>
<details>
<summary>Types</summary>

```cpp
// A type that defines how Bamboo's builtin functions are implemented.
typedef std::function<void(std::string, BambooVM* vm)> BuiltinFunction;
// A type that defines what a `Substack` is.
typedef std::list<Token> Substack;
struct RawToken;
struct BambooVM;
struct Token;
class<T> LimitedList;
```

</details>

#### `struct BabooVM`

This structure represents the "VM" that Bamboo runs all of its code on.
It contains debug information, aswell as all the tokens for a certain file.

```cpp
struct BambooVM
{
    // name of the file the VM was created for
    // used for debug
    std::string fileVMBelongsTo = "none";

    // All push/pop operations use this to push / pop
    std::list<Token> stack;

    // Contains all of the named values for this VM
    std::unordered_map<std::string, Token*> namedValues;

    // This is where the interpreter will look when loading librarys
    std::string libdir = "~/.bamboo/libs/";

    // Debug information, used to trace error calls
    std::vector<std::string> traceback;

    // Arguments passed to the VM
    std::vector<std::string> argv;

    // Librarys that have alread been imported and loaded
    // Used to prevent circular imports, and loading a file more than once
    std::list<std::string> imports;

    // C++ librarys that have already been injected
    // Used to prevent loading the library more than once
    std::list<std::string> injects;

    // Number of arguments passed to VM
    int argc;

    // The VM that caused this one to be created. Aka the "parent" VM.
    BambooVM* callee = this;

    // returns a copy of this VM
    inline BambooVM copy();

    // Returns a copy of this VM, but with a empty stack
    // and the child VM's callee pointing towards this VM
    inline BambooVM makeChild();
};
```

#### `Struct RawToken;`

```cpp
struct RawToken {
    // The line where the `content` was found on the file the `content` was extracted from.
    int line;
    //  A string containg the data that needs to be turned into a token. This string can represent a integer, or string.
	std::string content;
};
```
A structure used to represent a token that has not been given any type / converted to a `Token`.

#### `Struct token;`

The token class, everything in Bamboo is a token.

```cpp
struct Token
{
    /*
    Wether or not this token is a raw string.
    A raw string is a string that has spaces inside of it.
    */
    bool rawString;

    /*
    std::variant that holds the actual data this token contains.
    */
    std::variant<int, std::string, std::list<Token>> _data;

    /*
    The line on the file from which this token was read from.
    */
    int location;

    /* ... */
};
```
<details>
<summary>Methods</summary>

- `Token(std::string val, bool rawString = false, int line = -1):_data(val), rawString(rawString), location(line) {}`

    Constructs a `Token` object of type `TOKEN_TYPE_STR`, and intializes the tokens members

    Parameters:
    - `val` - The string that this token will hold
    - `rawString` - Wether or not this token is a raw string
    - `line` - The line on the file from which this token was extracted

    <details>
    <summary>Overloads</summary>

    ```cpp
    // construct a token with a integer
    Token(int val, int line = -1)
    :_data(val), location(line) {}

    // construct a token with a substack
    Token(Substack val, int line = -1)
    :_data(val), location(line) {}
    ```
    </details>

- `Token Token::fromRawData(std::string data, int line)`

    Returns a new token object from the given data.
    *Note that this is a static function.*

    Parameters:
    - `data` - The value that the token should hold.
    - `line` - The line number from the file this token was read from.

- `std::string getContent()`

    Returns the content of `Token::_data` as a string (if not already a string).
    If it is a substack, it will return "\<substack>"

- `inline .bool isRawString()`

    Returns wether the current token is a raw string or not.

- `inline void setRawString(bool value)`

    Sets the token's `rawString` member to the provided value.

    Paramters:
    - `value` - The `rawString` member of the current token is set to this.

- `inline void getType()`

    Returns the current tokens type represented as an integer

- `inline void getTypeAsString()`

    Returns the current tokens type represented as a string.

</details>

#### `class<T> LimitedList`

This class is used only in `Utils::error()`, for a specific function of the context builder.
It's a regular `std::list<Token>` class, except that there is a limit to the number of elements it holds at once.
If adding an element to the list exceeds the limit, it pops the front item off the list.

```cpp
template <class T>
class LimitedList {

    public:
        // Internal list that contains all of the elements
        std::list<T> _internal;

        // The maximum number of elements allowed in the list
        int limit;

        LimitedList(int limit)
        : limit(limit) {}

        /* ... */
};
```
<details>
<summary>Methods</summary>

- `inline void push(T item);`

    Pushes `item` to the list.

    Parameters:
    - `item` - the item to be pushed to the list

- `inline void pop();`

    Pops the item currently at the back of the list

</details>

---
### utils.hpp
---

#### `class Utils`

A utility class, it contains functions that are generally helpfull when working with bamboo.

```cpp
class Utils {
    public:
    // Utils has no members...

    /* ... */
};
```
<details>
<summary>Methods</summary>

- `static std::vector<std::string> split(std::string s);`

    splits a string by spaces and brackets

    Parameters:
    - `s` - The string to be split

- `static void strReplace(std::string &str, std::string substr, std::string replaceWith);`

    Replaces a substring in a string with `replaceWith`

    Parameters:
    - `str` - The string that has its contents replaced
    - `substr` - The substring that will be replaced
    - `replaceWith` - The string that replaces `substr`

- `static void coutStack(Substack vec);`

    Prints a stack.

    Parameters:
    - `vec` - The substack to print

- `static void coutStackWithColors(Substack stack);`

    Prints a stack with colors.

    Parameters:
    - `vec` - The substack to print

- `static void error(std::string message, BambooVM* vm = 0, Token token = 0);`

    Prints an error message, highlights the token that caused the error, and exits.

    Parameters:
    - `message` - The error message
    - `vm` - The VM the error orginated from.
    - `token` - The token that caused the error

- `static void warn(std::string message);`

    Prints a warning

    Parameters:
    - `message` - The message to display.

</details>

---
## interpreter.hpp

#### `inline void pushValueOfToken(Token &token, BambooVM* vm);`

Checks wether the string value of a token is a refference to a named value, stack position, and pushes the correct value to the stack. Or it checks of the token is calling a named value, and executes it with any given paramters.

Parameters:
- `token` - The token to be executed / pushed to the stack
- `vm` - The VM that `token` should be executed on / pushed to.

#### `void interpret(Token code, BambooVM* vm);`

Interprets the given token on the provided VM

Parameters:
- `code` - The token to be interpreted
- `vm` - The VM that `code` is executed on

<details>
<summary>Overloads</summary>

```cpp
void interpret(Substack code, BambooVM* vm);
```

</details>

## parser.hpp

#### `inline Token parseNextToken(std::fstream &file);`

Returns the next token that can be parsed from the provided file.

Parameteres:
- `file` - The file to read a token from.

#### `void parse(std::string fileName, BambooVM &fileVM);`

Parses a file, and runs every token found inside of it

Parameters:
- `fileName` - The name of the file to parse.
- `fileVM` - The VM to be used for this file. This will be the Vm that all child VM's are created from. ( Aka the first VM )
