# Tutorial-by-Examples

First off, any time a stack *(a stack is different from code)* is illustrated in these examples, it's shown with the following syntax:

```
{ ... } - Stack

{ 6 } - A stack with the value 6 in it

{ { ... } } - A stack with a substack in it

{ 5(myNum) } - A stack with the value 5 in it, and a named value called `myNum` references that 5.

{ { 10 }(myTen) } - A stack with a substack in it, which has the value 10 in it, and the named value myTen that references the substack.
```

## Terms

- "Substack" refers to a stack contained within another stack.
- "Named Value" refers to a name that has a corresponding value token.
- "Builtin" refers to a function that is built in to the interpreter.

## Hello, World!

```c
"Hello, Verve!" #print
```

**Output:**
```
Hello, Verve!
```

**Per-token:**
1. The string `"Hello, Verve!"` is pushed to the stack
2. The builtin `#print` is called out, which prints the previous token the the console.

## Named Values

Variables don't exist in Bamboo, rather "named values" are used.

Pretty much, you attach a name to a value on the stack, which in a nutshell is a pointer to the index of that value.

```c
myNumber 5 #def
```

**Output stack:**
```c
{ 5(myNumber) }
```

**Per-token**:
1. The string `myNumber` is pushed to the stack.
2. The number `5` is pushed to the stack.
3. The builtin `#def` is called, which pops the string `myNumber` and creates a named value that points to the `5` on the stack.

To reference a named value, use the `.` prefix.

```c
myNumber 5 #def
4 .myNumber #+
```

**Output stack:**
```c
{ 5(myNumber) 9 }
```

**Per-token**:
1. The string `myNumber` is pushed to the stack.
2. The number `5` is pushed to the stack.
3. The builtin `#def` is called, which pops the string `myNumber` and creates a named value that points to the `5` on the stack.
4. The number `4` is pushed to the stack.
5. A reference to `myNumber` is found, and a copy of the value it references is pushed to the stack.
6. The builtin `#+` is called, which pops the previous two tokens (`4` and `5`), then the sum is pushed to the stack.

## Math

```
3 7 #+
```

**Output stack:**
```c
{ 10 }
```

The output stack of the above code is `{ 10 }`.

1. The number `3` is pushed to the stack.
2. The number `7` is pushed to the stack.
3. The builtin `#+` is called, `3` and `7` are added together, then both popped off the stack, then the sum of each are pushed back on the stack.

Other math builtins are `#-`, `#*`, `#/`, and `#%`.

## If statements and While loops

# If statements

In Bamboo, we use if statements to control wether or not a token is pushed to the stack.
Take for example, the following code:
```c
input #input #stoi #def

if .input {
    a
} else {
    b
}
end
```

Running this will prompt for a user input, and if that input can be considered true, then `a` is pushed to the stack, else `b` is pushed to the stack.

**Output stack:**
```c
{ 10(input), a }
```

1. `input` is pushed to the stack
2. `#input` is called, which pushes the user input to the stack. In our case, `10` was pushed to the stack because that is what we inputted.
3. `#stoi` is called, which turns the `10` on the stack from a string to an integer. *#input pushes user input as a string, and cannot implicity convert the value to the user's desired type!*
4. `#def` is called, which associates the name `input` with the `10` on the stack. it pop's the `input` off the stack after.
5. The interpreter hits the `if`, which prompts it to move to the next token and determine wether its true of false. Since that token is a named value, it looks at the value associated with it, which is `10` in this case.
6. The interpreter determines it was true, and executes all of the commands / values in the substack after it on the current vm. In this case, that would be the main VM ( the VM that stores ever other VM and stack ) *This causes `a` to be pushed to the stack*
7. The interpreter then skips all tokens it hits until the `end` keyword is reached, which tells the interpreter that the if/else statement is over.

# While loops

While loops are pieces of code that run until a given condition is false. Here is a simple program demonstrating that.

```c
n 0 #def

while { .n 10 #!= } {

    value

    .n #print
    1 #+ n #redef
}
```

**Output stack:**
```c
{ 10(n), value, value, value, value, value, value, value, value, value, value, }
```

1. We define `n` to point to the `0` on the stack.
2. The interpreter hits the `while` keyword, interprets the substack next to it on a child VM created from the main vm. It then checks for wether the value on the BACK of that VM's stack is true or not.
3. If the value was true, it executes the substack after the condition repeatedly until that condition becomes false. Note that the code executed will have all of its values pushed to the stack the while loop was executed from.

## "Functions"

Like variables, functions also don't exist in Bamboo. Rather, it's a named value that points to a substack.

The substack is executed when the "function" is called.

```c
myFunc { 21 9 #+ } #def
```

**Output stack:**
```c
{ { 21 9 #+ }(myFunc) }
```

To call these, the : prefix is used, and the provided named value is executed.

```c
myFunc { 21 9 #+ #return } #def

:myFunc
```

**Output stack:**
```c
{ { 21 9 #+ }(myFunc) 30 }
```

2. The string `myFunc` is pushed to the stack.
1. The substack `{ 21 9 + }` is pushed to the stack.
3. The builtin `#def` is called, which pops the string `myFunc` and makes a named value called `myFunc` that points to the substack in step 1.
4. `:myFunc` is seen, then the named value `myFunc` is executed. This causes the substack `myFunc` is refferencing, to be interpreted on its own stack ( memory pool ). To return any value to the stack that the function was called from, `#return` is used to push the previous token to the stack the function was called from.

## Function Arguments

To define a function that takes in arguments, use the following syntax:

```c
say-hello { {first-name last-name}
	{ "Hello, " .first-name " " .last-name } #+ #print
} #def

{ "Payton" "Martian" } !say-hello
```

Note how the call prefix is not `:` anymore. To pass args, `!` is used.

> Note that `!` *can* be used to call a function without passing args, but it first must check if the previous item on the stack is a substack, if so, those are passed as args.
>
> This leads to some gotchas, and is also slower than using : because ! has to check for the substack.
>
> In general, use : ANY time you are not passing arguments. If you are passing arguments, use !

## Stack References

> First off, you should never need to use stack references. These are iffy and are not recommended, although they exist for niche use-cases.

A stack reference allows one to copy the value of a token on the stack given it's index relative to the current index.

To reference the item right before the current token, use `.$-1`. For the second item, `.$-2`, third is `.$-3`, etc.

```c
1 8 .$-2 #+ #+
```

**Output stack:**
```c
{ 10 }
```

**Per-token:**
1. The number `1` is pushed to the stack.
2. The number `8` is pushed to the stack.
3. The stack reference to `$-2` is seen, and `1` is pushed to the stack.
4. The builtin `#+` is called, which adds `8` and `1`.
5. The builtin `#+` is called again, which adds `1` and `9`.

## Imports

In bamboo, we can import other bamboo files (.bam/bl) into your main file by calling:

```c
"file.bam" #load
```

Here's an example from the offical standard library:

```c
; Don't worry about this line yet, we'll get there!
"clibs/random.so" #inject

randrange { {lower upper}
; lower + (std::rand() % (upper - lower + 1))
    .lower
    #rand
    .upper
    .lower
    1
    #+
    #-
    #%
    #+
    #return
} #def

random {
    #rand
    #return
} #def

seed { {user-seed}
    ; if the seed is -1
    ; then use #get-time as the seed
    if { .user-seed -1 #== }
    {
        #get-time user-seed #redef
    }
    end

    .user-seed #seed
} #def
```

Normally, this code would define 3 substacks: `seed`, `randrange`, and `random`. These are not useful to us as we dont have access to these
named values in our file. In order to use these, we have to load them into our `main.bam` (or whatever file you're currently working in):

```c
; main.bam

"libs/random.bam" #load

; use -1 to use `(int)time(NULL)` as the seed!
{-1} !seed
:random
#print
```

All substacks and named valued defined within `math.bam` can now be seen and used within `main.bam`

**Output**
```c
; In your actual implemenetation, this would be a random number.
; for us, we got 1499103083.
1499103083
```

This also loads everything withing `random.bam` into the stack of the file you're loading it into.

**`main.bam` stack**
```c
{ { { lower, upper, } .lower, #rand, .upper, .lower, 1, #+, #-, #%, #+, #return, }(randrange) { #rand, #return, }(random) { { user-seed, } if, { .user-seed, -1, #==, } { #get-time, user-seed, #redef, } end, .user-seed, #seed, }(seed) 1499103083, }
```

## .so Injects

Sometimes it's better for someone to make their Bamboo library in C/C++. This can be true for many reasons, but either way, if you encounter a library that uses .so files, this is how you import it:

```c
"path/to/sofile.so" #inject
```

Note that when a file is injected, it's functions will typically be builtins rather than not. Although this may not be true 100% of the time, it's mostly up to the developer of the library. Lets use the standard library's `random.cpp` as an example:

```c
; Make sure you run `python3 install.py` after cloning Bamboo for this to properly work!

"random.so" #inject
```

After injecting the file, you now have accsess to the following C++ functions
from within Bamboo:

```c
; The implementation of each of these functions can be found inside of
; the source file.
#get-time
#seed
#rand
```

Running these commands will print a random number.

## Output
```c
{ 1683662107, 1613529615, }
```

The first item in the stack is the current time, returned by `#get-time`. The second item is the random number we generated with `#rand`

# Something to note about injects loads

Injecting C++ files and loading .bam files is **VERY** slow. Try to avoid doing this when possible (But don't repeat yourself!).
