add_rules("mode.debug", "mode.release")
add_syslinks("dl")

target("bamboolib")
    set_kind("shared")
    add_files("src/bamboolib/*.cpp")
    set_languages("cxx20")

target("bamboo")
    set_kind("binary")
    add_files("src/*.cpp")
    add_deps("bamboolib")
    set_runargs("$(curdir)/example.bam", "-s")
    if is_mode("release") then
        set_symbols("hidden")
        set_optimize("fastest")
        set_strip("all")
    end
    set_languages("cxx20")
    on_run(function(target)
        os.cd("$(projectdir)/")
        c = "./$(buildir)/$(os)/" .. os.arch() .. "/release/bamboo run example.bam -s"
        os.exec(c)
    end)

target("install")
    on_build(function(_) end)
    on_link(function(_) end)
    on_run(function(target)
        os.cp("./$(buildir)/$(os)/" .. os.arch() .. "/release/bamboo", "~/.bamboo/bin/bamboo")
        os.cp("./$(buildir)/$(os)/" .. os.arch() .. "/release/libbamboolib.so", "~/.bamboo/bin/libbamboolib.so")
    end)

