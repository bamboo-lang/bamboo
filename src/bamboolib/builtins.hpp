#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <dlfcn.h>
#include <unistd.h>
#include <filesystem>
#include <variant>

#include "interpreter.hpp"
#include "parser.hpp"
#include "utils.hpp"

#ifndef __bamboolib_builtins_cpp__
#define __bamboolib_builtins_cpp__

// Built-in functions
std::unordered_map<std::string, BuiltinFunction> builtins {
    { "def", [] (std::string val, BambooVM* vm) {
        auto itr    = vm->stack.end();
        auto value  = (--itr);
        auto name   = (--itr);
        vm->namedValues[std::get<std::string>(name->_data)] = &(*value);
        vm->stack.erase(itr);
    }},
    { "type-of", [] (std::string val, BambooVM* vm) {
        auto itr = --vm->stack.end();
        vm->stack.push_back(
            HOLDS<int>(itr->_data) ?
                std::string("int") : HOLDS<std::string>(itr->_data) ?
                    std::string("str") : std::string("<substack>")
        );
    }},
    { "redef", [] (std::string val, BambooVM* vm) {
        // key exist?
        if (vm->namedValues.find(std::get<std::string>(vm->stack.back()._data)) == vm->namedValues.end())
        { Utils::error("#redef requires that a named value exist before assigning a new value.", vm); }

        // stack.end() returns an itr to the last element of the list + 1
        // call --vm->stack.end(); to get an itr to the actual last element (node)
        auto tailItr = --vm->stack.end();

        // get the name string.
        std::string name = std::get<std::string>(tailItr->_data);

        // bring iterator back one, to the new value;
        tailItr--;

        // replace the old iterator value to the new iterator value
        // that points to the new value
        *vm->namedValues[name] = *tailItr;
        vm->stack.pop_back(); // pop name
        vm->stack.pop_back(); // pop value

    }},
    {"exists", [] (std::string val, BambooVM* vm) {
        std::string name = std::get<std::string>(vm->stack.back()._data);
        vm->stack.pop_back(); // pop name
        vm->stack.push_back( vm->namedValues.find(name) != vm->namedValues.end() );
    }},
    // io and debug
    { "print", [] (std::string val, BambooVM* vm) {
        std::cout << vm->stack.back().getContent() << std::endl;
    }},
    { "printnf", [] (std::string val, BambooVM* vm) {
        std::cout << vm->stack.back().getContent();
    }},
    // this was added by payton on verve's machine
    { "input", [] (std::string val, BambooVM* vm) {
        std::string input;
        std::cin >> input;
        vm->stack.push_back(Token(input));
    }},
    { "stoi", [] (std::string val, BambooVM* vm) {
        if (!HOLDS<std::string>(vm->stack.back()._data))
            Utils::error("Cannot convert non string to integer. Token must be of type string", vm, vm->stack.back());

        try
        {
            vm->stack.back() = std::stoi(std::get<std::string>(vm->stack.back()._data));
        }
        catch (const std::exception& e)
        {
            Utils::error("String could not be convereted to integer.", vm, vm->stack.back());
        }

    }},
    { "stack", [] (std::string val, BambooVM* vm) {
        Utils::coutStack(vm->stack);
        std::cout << std::endl;
    }},
    { "clear", [] (std::string val, BambooVM* vm) {
        vm->stack.clear();
    }},
    // loading external symbols
    { "load", [] (std::string val, BambooVM* vm) {
        std::string file = std::get<std::string>(vm->stack.back()._data);
        Token copy = vm->stack.back();
        vm->stack.pop_back(); // pop file name thats going to be loaded

        if (std::filesystem::exists(std::filesystem::current_path().string() + '/' + file))
            file = std::filesystem::current_path().string() + '/' + file;
        else if (std::filesystem::exists(vm->libdir + "/libs/" + file))
            file = vm->libdir + "/libs/" + file;
        else
            Utils::error("lib path " + file + " does not exist.", vm, copy);

        // Check if the file to load has already been loaded
        if (std::find(vm->imports.begin(), vm->imports.end(), file) != vm->imports.end())
            return;

        parse(file, *vm);
        vm->imports.push_back(file);
    }},
    { "inject", [] (std::string val, BambooVM* vm) {
        void (*inject_func)(BambooVM* vm, std::unordered_map<std::string, BuiltinFunction>* builtins);
        std::string file = std::get<std::string>(vm->stack.back()._data);
        vm->stack.pop_back(); // Pop the file name that is being loaded.

        if (std::filesystem::exists(std::filesystem::current_path().string() + '/' + file))
            file = std::filesystem::current_path().string() + '/' + file;
        else if (std::filesystem::exists(vm->libdir + "clibs/" + file))
            file = vm->libdir + "clibs/" + file;
        else
            Utils::error("clib path " + file + " does not exist.", vm);

        // Check if the file to load has already been loaded
        if (std::find(vm->injects.begin(), vm->injects.end(), file) != vm->injects.end())
            return;

        void* library = dlopen(file.c_str(), RTLD_LAZY);
        if (library != NULL)
        {
            *(void **)(&inject_func) = dlsym(library, "inject");
            inject_func(vm, &builtins);
            // Calling dlclose() causes a segfault, I'm pretty sure that is because the closure that is pushed to `builtins` by inject
            // is unloaded after calling dlclose, meaning that is "exists" but not really.
            // dlclose(library);
        }
        vm->injects.push_back(file);
    }},
    // Pop
    { "pop", [] (std::string val, BambooVM* vm) {
        vm->stack.pop_back();
    }},
    // Push item to callee stack
    { "push", [] (std::string val, BambooVM* vm) {
        vm->
            callee->stack.push_back(vm->stack.back());
    }},
    // Argv/Argc
    { "argc", [] (std::string val, BambooVM* vm) {
        vm->stack.push_back(Token(vm->argc));
    }},
    { "argv", [] (std::string val, BambooVM* vm) {
        auto args = Substack();
        for (const std::string arg : vm->argv) args.push_back(Token(arg));
        vm->stack.push_back(Token(args));
    }},
    // Get in "list"
    { "get", [] (std::string val, BambooVM* vm) {
        auto itr = vm->stack.end();
        int index =std::get<int>( (--itr) ->_data);
        std::string listName = std::get<std::string>( (--itr) ->_data);

        vm->stack.pop_back(); // Pop listName
        vm->stack.pop_back(); // Pop index

        auto nv = vm->namedValues.at(listName);

        if (!HOLDS< Substack >(nv->_data))
            Utils::error("cannot call #get-list on a non-substack item", vm);

        int i = 0;
        for (Token &t : std::get<Substack>(nv->_data))
        {
            if (i == index)
            {
                vm->stack.push_back(t);
                return;
            }
            i++;
        }

        Utils::error("index out of range (index: " + std::to_string(index) + " list: " + listName + ")" );
    }},
    // return to callee stack and push back prev item
    // special logic in the interpreter is needed here
    { "return", [] (std::string val, BambooVM* vm) {
        vm->callee->stack.push_back(vm->stack.back());
        throw 0;
    }},
    // Exit to the callee
    // Does not return a value
    // This entry only exist so
    // there is no error checking if the function existsd
    { "exit", [] (std::string val, BambooVM* vm) {
        throw 0;
    }},
    {"not", [] (std::string val, BambooVM* vm) {

        if (!HOLDS<int>(vm->stack.back()._data))
        { Utils::error("#not requires an int token before it.", vm); }
        bool operand = (bool)std::get<int>(vm->stack.back()._data);

        // pop orginall value
        vm->stack.pop_back();

        // push new value
        vm->stack.push_back(!operand);

    }},
    // Equals
    { "==", [] (std::string val, BambooVM* vm) {

        // create iterators
        auto itr    = --vm->stack.end();
        auto a      = itr--; // vm->stack.size() - 1
        auto b      = itr--; // vm->stack.size() - 2
        // itr = vm->stack.size() - 3

        if (a->getType() != b->getType())
            Utils::error("#== requires two operands of the same type", vm);

        int equals;

        if (HOLDS<std::string>(a->_data))
            equals = std::get<std::string>(a->_data) == std::get<std::string>(b->_data);
        else
            equals = std::get<int>(a->_data) == std::get<int>(b->_data);

        // Get the sum
        vm->stack.pop_back(); // This pops bufferLength-1
        vm->stack.pop_back(); // This pops bufferLength-1, which before the previous pop_back() was bufferLength-2
        vm->stack.push_back(equals);
    }},
    { "!=", [] (std::string val, BambooVM* vm) {

        // create iterators
        auto itr    = --vm->stack.end();
        auto a      = itr--; // vm->stack.size() - 1
        auto b      = itr--; // vm->stack.size() - 2
        // itr = vm->stack.size() - 3

        if (a->getType() != b->getType())
            Utils::error("#== requires two operands of the same type.", vm);

        int equals;

        if (HOLDS<std::string>(a->_data))
            equals = std::get<std::string>(a->_data) != std::get<std::string>(b->_data);
        else
            equals = std::get<int>(a->_data) != std::get<int>(b->_data);

        vm->stack.pop_back(); // This pops bufferLength-1
        vm->stack.pop_back(); // This pops bufferLength-1, which before the previous pop_back() was bufferLength-2
        vm->stack.push_back(equals);
    }},
    // Math
    { "+", [] (std::string val, BambooVM* vm) {
        auto itr = vm->stack.end();
        auto b = --itr;
        Substack::iterator a;

        if (HOLDS<int>(b->_data))
        {
            if (HOLDS<int>((--itr)->_data))
            {
                int res = std::get<int>(b->_data) + std::get<int>(itr->_data);
                vm->stack.pop_back();
                vm->stack.pop_back();
                vm->stack.push_back(res);
            }
            else if (HOLDS<std::string>(itr->_data))
            {
                std::string res = std::get<std::string>(itr->_data) + std::to_string(std::get<int>(b->_data));
                vm->stack.pop_back();
                vm->stack.pop_back();
                vm->stack.push_back(res);
            }
            else
                Utils::error("cannot add substack to integer.", vm);
        }
        else if (HOLDS<std::string>(b->_data))
        {
            if (HOLDS<int>((--itr)->_data))
            {
                std::string res = std::to_string(std::get<int>(itr->_data)) + std::get<std::string>(b->_data);
                vm->stack.pop_back();
                vm->stack.pop_back();
                vm->stack.push_back(res);
            }
            else if (HOLDS<std::string>(itr->_data))
            {
                std::string res = std::get<std::string>(itr->_data) + std::get<std::string>(b->_data);
                vm->stack.pop_back();
                vm->stack.pop_back();
                vm->stack.push_back(res);
            }
            else
                Utils::error("cannot add substack to string.", vm);
        }
        else // it must be a substack of values to add
        {
            BambooVM valuesVM = vm->makeChild();

            // Interpret the values so any named values / logic is processed first
            interpret(std::get<Substack>(b->_data), &valuesVM);

            // start adding stuff to the first value
            auto itr2 = valuesVM.stack.begin();


            if (HOLDS<int>(itr2->_data))
            {
                int sum = 0;
                for (; itr2 != valuesVM.stack.end(); itr2++)
                {
                    if (HOLDS<Substack>(itr2->_data))
                        Utils::error("Cannot add substack to integer in grouped addition");
                    else if (HOLDS<std::string>(itr2->_data))
                    {
                        try {
                            sum += std::stoi(std::get<std::string>(itr2->_data));
                        } catch (const std::exception& e) {
                            Utils::error("Cannot add non-numeric string to integer in grouped addition", vm, *itr2);
                        }
                    }
                    else
                    {
                        sum += std::get<int>(itr2->_data);
                    }
                }

                vm->stack.pop_back();
                vm->stack.push_back(sum);
            }
            else if (HOLDS<std::string>(itr2->_data))
            {
                std::string sum;

                for (; itr2 != valuesVM.stack.end(); itr2++)
                {
                    if (HOLDS<Substack>(itr2->_data))
                        Utils::error("Cannot add substack to string in grouped addition.", vm);

                    try {

                        sum += std::to_string(std::get<int>(itr2->_data));

                    } catch (std::bad_variant_access e) {

                        sum += std::get<std::string>(itr2->_data);
                    }

                }

                vm->stack.pop_back();
                vm->stack.push_back(sum);

            }
        }
    }},
    { "-", [] (std::string val, BambooVM* vm) {

        // create iterators
        auto itr    = --vm->stack.end();
        auto b      = itr--; // vm->stack.size() - 1
        auto a      = itr--; // vm->stack.size() - 2
        // itr = vm->stack.size() - 3

        // Make sure the previous two items are ints
        if (
            ! HOLDS<int>(a->_data) ||
            ! HOLDS<int>(b->_data)
        )
        { Utils::error("#- requires two int operends before it.", vm); }

        // Get the difference
        int difference = std::get<int>(a->_data) - std::get<int>(b->_data);
        vm->stack.pop_back(); // This pops bufferLength-1
        vm->stack.pop_back(); // This pops bufferLength-1, which before the previous pop_back() was bufferLength-2
        vm->stack.push_back(difference);
    }},
    { "/", [] (std::string val, BambooVM* vm) {

        // create iterators
        auto itr    = --vm->stack.end();
        auto b      = itr--; // vm->stack.size() - 1
        auto a      = itr--; // vm->stack.size() - 2
        // itr = vm->stack.size() - 3

        // Make sure the previous two items are ints
        if (
            ! HOLDS<int>(a->_data) ||
            ! HOLDS<int>(b->_data)
        )
        { Utils::error("#/ requires two int operends before it.", vm); }

        // Get the result
        int result = std::get<int>(a->_data) / std::get<int>(b->_data);
        vm->stack.pop_back(); // This pops bufferLength-1
        vm->stack.pop_back(); // This pops bufferLength-1, which before the previous pop_back() was bufferLength-2
        vm->stack.push_back(result);
    }},
    { "*", [] (std::string val, BambooVM* vm) {

        // create iterators
        auto itr    = --vm->stack.end();
        auto b      = itr--; // vm->stack.size() - 1
        auto a      = itr--; // vm->stack.size() - 2
        // itr = vm->stack.size() - 3

        // Make sure the previous two items are ints
        if (
            ! HOLDS<int>(a->_data) ||
            ! HOLDS<int>(b->_data)
        )
        { Utils::error("#* requires two int operends before it.", vm); }

        // Get the product
        int product = std::get<int>(a->_data) * std::get<int>(b->_data);
        vm->stack.pop_back(); // This pops bufferLength-1
        vm->stack.pop_back(); // This pops bufferLength-1, which before the previous pop_back() was bufferLength-2
        vm->stack.push_back(product);
    }},
    { "%", [] (std::string val, BambooVM* vm) {

        // create iterators
        auto itr    = --vm->stack.end();
        auto b      = itr--; // vm->stack.size() - 1
        auto a      = itr--; // vm->stack.size() - 2
        // itr = vm->stack.size() - 3

        // Make sure the previous two items are ints
        if (
            ! HOLDS<int>(a->_data) ||
            ! HOLDS<int>(b->_data)
        )
        { Utils::error("% requires two int operends before it.", vm); }

        // Get the result
        int result = std::get<int>(a->_data) % std::get<int>(b->_data);
        vm->stack.pop_back(); // This pops bufferLength-1
        vm->stack.pop_back(); // This pops bufferLength-1, which before the previous pop_back() was bufferLength-2
        vm->stack.push_back(result);
    }},
    { ":", [] (std::string val, BambooVM* vm) {
        auto itr    = vm->stack.end();

        if ( !HOLDS<Substack>(itr->_data) )
        { Utils::error("multi-def requires a substack of names. EX: `{values} {names} #:`", vm); }

        auto names  = std::get<Substack>( (--itr) ->_data);

        if ( !HOLDS<Substack>(itr->_data) )
        { Utils::error("multi-def requires a substack of value for the substack of names. EX: `{values} {names} #:`", vm); }

        auto values = std::get<Substack>( (--itr) ->_data);

        if (names.size() != values.size())
        { Utils::error("multi-def requires both names and values substack be of the same size.", vm); }

        vm->stack.pop_front(); // pop the values substack

        // make itr point to the values substack to
        // avoid allocating memory for a new iterator
        itr = --values.end();

        // iterate in reverse because we have no idea
        // where the users values for the names starts
        // so just go backwards for each name.
        for (auto i = names.rbegin(); i != names.rend(); ++i)
        {
            vm->namedValues[ std::get<std::string>(i->_data) ] = &*(itr--);
        }

        vm->stack.pop_back();   // Pop the names substack
    }},
    {">", [] (std::string val, BambooVM* vm) {
        auto itr = vm->stack.end();
        Token b = *itr--;
        Token a = *itr--;

        if (!(HOLDS<int>(a._data) && HOLDS<int>(b._data)))
        { Utils::error("#> requires both operands be of type int."); }

        vm->stack.push_back( (std::get<int>(a._data) > std::get<int>(b._data)) );
    }},
    {"<", [] (std::string val, BambooVM* vm) {
        auto itr = vm->stack.end();
        Token b = *itr--;
        Token a = *itr--;

        if (!(HOLDS<int>(a._data) && HOLDS<int>(b._data)))
        { Utils::error("#< requires both operands be of type int."); }

        vm->stack.push_back( (std::get<int>(a._data) < std::get<int>(b._data)) );
    }},
    {">=", [] (std::string val, BambooVM* vm) {
        auto itr = vm->stack.end();
        Token b = *itr--;
        Token a = *itr--;

        if (!(HOLDS<int>(a._data) && HOLDS<int>(b._data)))
        { Utils::error("#>= requires both operands be of type int."); }

        vm->stack.push_back( (std::get<int>(a._data) >= std::get<int>(b._data)) );
    }},
    {"<=", [] (std::string val, BambooVM* vm) {
        auto itr = vm->stack.end();
        Token b = *itr--;
        Token a = *itr--;

        if (!(HOLDS<int>(a._data) && HOLDS<int>(b._data)))
        { Utils::error("#<= requires both operands be of type int."); }

        vm->stack.push_back( (std::get<int>(a._data) <= std::get<int>(b._data)) );
    }}
};

#endif
