#include <fstream>
#include <vector>
#include <list>
#include <iostream>

#include "interpreter.hpp"
#include "parser.hpp"
#include "utils.hpp"


#ifndef __bamboolib_parser_cpp__
#define __bamboolib_parser_cpp__


Token readNextToken(std::fstream &file);

// Variables required by `parseNext` that need to live outside of the function
// do not use these for anything.
char P_currentFileCharacter;
int P_lineNum = 1;
std::vector<char> P_charBuffer(100);

#define toString(v) std::string(v.begin(), v.end())

// Reads a substack starting from the first { into the provided buffer.
// Writes to a buffer because its faster than copying a token / substack object
void readSubstackToBuffer(std::fstream &file, std::list<Token>* to)
{
    Token t = NULL;

    try {

        while(true)
        {
            t = readNextToken(file);

            if (HOLDS<std::string>(t._data) && std::get<std::string>(t._data) == "}")
                    return;

            to->push_back(t);

        }


    } catch (int e) {
        if (e == EOF)
            throw EOF;
    }

}

inline void readStringToBuffer(std::fstream &file, std::vector<char> *to)
{
    P_charBuffer.push_back('"');
    while((P_currentFileCharacter = file.get()))
    {
        switch(P_currentFileCharacter)
        {
            case EOF:
                throw std::runtime_error("EOF hit inside of string.");
            case '"':
                to->push_back('"');
                return;
            case '\n':
                P_lineNum++;
            case '\r':
            case '\t':
                continue;
            default:
                to->push_back(P_currentFileCharacter);
        }
    }
}

// returns the next token that can be parsed from the provided file.
Token readNextToken(std::fstream &file)
{
    // we want to start with a empty buffer each time
    P_charBuffer.clear();
    std::list<Token> buf;


    while ((P_currentFileCharacter = file.get()))
    {
start:  switch(P_currentFileCharacter)
        {
            case EOF:
                if (P_charBuffer.empty()) throw EOF;
                return Token::fromRawData(toString(P_charBuffer), P_lineNum);
            case ';':
                // if it hits the start of a comment, everything up until the next new line is unwanted
                if (P_charBuffer.empty())
                {
                    while((P_currentFileCharacter = file.get()) != '\n' && P_currentFileCharacter != EOF);
                    continue;
                }
                return Token::fromRawData(toString(P_charBuffer), P_lineNum);
            case '"':
                // if the buffer is empty we can go ahead and
                // read the string to the buffer
                // either way since the string is a new token, we would
                // need to return the current buffer. As such we can just
                // put the unget and read into a single if / else
                if (P_charBuffer.empty())
                    readStringToBuffer(file, &P_charBuffer);
                else
                    file.unget();

                return Token::fromRawData(toString(P_charBuffer), P_lineNum);
            case '{' :
                if (!P_charBuffer.empty())
                {
                    file.unget();
                    auto n = Token::fromRawData(toString(P_charBuffer), P_lineNum);
                    return Token::fromRawData(toString(P_charBuffer), P_lineNum);
                }

                readSubstackToBuffer(file, &buf);
                return Token(buf);
            case '}':
                if (P_charBuffer.empty())
                    return Token("}");

                file.unget();
                return Token::fromRawData(toString(P_charBuffer), P_lineNum);
            case ' ' :
                // Once it hit's a space, it returns the current buffer.
                // if the buffer is empty, then it progresses the current file character until
                // it hit's a non space, and then returns to the beginning of the while loop
                if (P_charBuffer.empty()) { while((P_currentFileCharacter = file.get()) == ' '); goto start; }
                return Token::fromRawData(toString(P_charBuffer), P_lineNum);
            case '\t':
            case '\r':
                continue;
            case '\n':
                if (P_charBuffer.empty()) { P_lineNum++; continue; }
                return Token::fromRawData(toString(P_charBuffer), P_lineNum++);
            default  :
                P_charBuffer.push_back(P_currentFileCharacter);
        }
    }

    throw -2;

}


void parse(std::string fileName, BambooVM &fileVM)
{
    bool skip = false;
    std::fstream file(fileName);

    Token t = 0;

    try {
outer:
        while (t = readNextToken(file), true)
        {
            if (HOLDS<std::string>(t._data))
            {
                Substack chain;

                try
                {
                    if (std::get<std::string>(t._data) == "if")
                    {

                        // Just add everything to a buffer until we hit the `end` keyword.
                        // the interpreter will do all type checks, and make sure that
                        // the statement is properly formed
                        do {
                            chain.push_back(t);

                            if (HOLDS<std::string>(t._data) && std::get<std::string>(t._data) == "end")
                                break;
                        } while ((t = readNextToken(file)), 1);


                        // skip the `end`
                        skip = true;

                        interpret(chain, &fileVM);
                    }
                    else if (std::get<std::string>(t._data) == "while")
                    {
                        chain.push_back(t);

                        Token condition = readNextToken(file);
                        Token code = readNextToken(file);


                        if (HOLDS<Substack>(condition._data) && HOLDS<Substack>(code._data))
                        {
                            chain.push_back(condition);
                            chain.push_back(code);
                            interpret(chain, &fileVM);
                            continue;
                        }
                        else
                        {
                            Utils::error("While loop requires a SUBSTACK condition and a SUBSTACK to execute. One or both of the condition and code are not of the Substack type.");
                        }
                    }
                }
                catch (int &e)
                {
                    if (e == EOF)
                    {
                        // Edge case right here
                        // if there is an EOF and the chain is 3 or more, then
                        // the control flow statement was at the end of the file and should still
                        // be interpreted before exiting
                        if (chain.size() >= 3)
                            interpret(chain, &fileVM);
                        else
                            Utils::error("Hit EOF inside of control flow. (If/Else/While)", &fileVM);
                        break;
                    }
                }
            }

            if (skip)
                skip = false;
            else
                interpret(t, &fileVM);

        }
    } catch (int e) {
        switch (e) {
            case EOF:
                Utils::warn("hit EOF");
                break;
            default:
                Utils::error("Token read hit outer throw, an unknown error has occurred");
        }
    }
}

#endif