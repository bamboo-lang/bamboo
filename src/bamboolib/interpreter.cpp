#include "types.hpp"
#include "utils.hpp"
#include "interpreter.hpp"
#include "parser.hpp"
#include "builtins.hpp"

#ifndef __bamboolib_interpreter_cpp__
#define __bamboolib_interpreter_cpp__

// Gets the value of the token, then pushes that value to the stack.
// The token provided must be a token type of `string`
inline void pushValueOfToken(Token &token, BambooVM* vm)
{
    // Get the val of the token
    std::string val = std::get<std::string>(token._data);

    // If the string has an ! at the start, it's calling a named substack.
    if (val.at(0) == '!')
    {
        std::string name = val.substr(1, val.size());
        if (vm->namedValues.find(name) == vm->namedValues.end())
            Utils::error("the named value `" + name + "` does not exist.", vm);
        else if (!HOLDS<Substack>(vm->namedValues[name]->_data))
            Utils::error("cannot execute non-substack. Named value `" + name + "` is a " + ( HOLDS<int>(vm->namedValues[name]->_data)  ? "integer." : "string."), vm);

        try
        {
            BambooVM substackMemory = vm->makeChild();
            // incase of args
            BambooVM argumentsVM = vm->makeChild();

            vm->traceback.push_back(name);

            // substack has paramters?
            if (
                HOLDS<Substack>(
                    std::get<Substack>(vm->namedValues[name]->_data).front()._data
                )
            )
            {
                // if it has paramaters, then we can perform the memory operations
                // to provide them

                // check for arguments
                if (!HOLDS<Substack>(vm->stack.back()._data))
                { Utils::error("non-fast named value call requires substack of arguments before it. EX: `{args} !call` or `:call` for no args", vm); }

                // copy arguments to variable so we can go ahead and pop them now
                Token arguments = std::get<Substack>(vm->stack.back()._data);

                vm->stack.pop_back();

                // interpret the arguments substack so that
                // any named values in there will be their actual values.
                // still starts arguments from the first element in the stack
                interpret( std::get<Substack>(arguments._data), &argumentsVM );

                auto argumentsItr = argumentsVM.stack.begin();

                // very pretty range expression LOL
                for ( auto &paramater : std::get<Substack> (
                            std::get<Substack>(vm->namedValues[name]->_data).front()._data
                        )
                    )
                {
                    // create iterator to paramter
                    substackMemory.namedValues.insert({std::get<std::string>(paramater._data), &*(argumentsItr++)});
                }
            }


            // execute substack with provided memory pool
            interpret(std::get<Substack>(vm->namedValues[name]->_data), &substackMemory);

            // pop arguments off VM

            // update traceback
            vm->traceback.pop_back();

        }
        catch (const std::exception &e)
        {
            Utils::error(std::string("an error occured whilst executing a substack. Error: ") + e.what(), vm);
        }
    }
    // If the string has a : at the start, it's calling a named substack without checking for arguments provided.
    else if (val.at(0) == ':')
    {
        std::string name = val.substr(1, val.size());

        if (vm->namedValues.find(name) == vm->namedValues.end())
            Utils::error("the named value `" + name + "` does not exist.", vm);
        else if ( !HOLDS<Substack>(vm->namedValues[name]->_data))
            Utils::error("cannot execute non-substack. Named value `" + name + "` is a " + (HOLDS<int>(vm->namedValues[name]->_data) ? "integer." : "string."), vm);

        try
        {
            vm->traceback.push_back(name);

            BambooVM substackMemory = vm->makeChild();

            // execute substack with provided memory pool
            interpret(std::get<Substack>(vm->namedValues[name]->_data), &substackMemory);

            vm->traceback.pop_back();
        }
        catch (const std::exception &e)
        {
            Utils::error(std::string("an error occured whilst executing a substack. Error: ") + e.what(), vm);
        }
    }
    // If the string has a . at the start, it's copying the value of a named value.
    else if (val.at(0) == '.')
    {
        if (val.at(1) == '$') // A stack reference
        {
            int change = std::stoi(val.substr(2, val.size()));

            if (change >= 0)
            { Utils::error("cannot make stack reference to self or future elements.", vm, token); }

            int count = 0;

            auto itr = vm->stack.end();

            // keep decremeneting the iterator until
            // its "position" is equal to the desired change
            while(count != change)
            {
                itr--;
                count--;
            }

            vm->stack.push_back(*itr);
        }
        else // A named value reference
        {
            std::string name = val.substr(1, val.size());

            if (vm->namedValues.find(name) == vm->namedValues.end())
            { Utils::error("named value `" + name + "` does not exist", vm, token); }

            vm->stack.push_back(*vm->namedValues[name]);
        }
    }
    // If the string has a # at the start, it's calling a builtin.
    else if (val.at(0) == '#')
    {
        std::string name = val.substr(1, val.size());
        if (builtins.find(name) == builtins.end())
            Utils::error("builtin `" + name + "` does not exist.", vm);

        BuiltinFunction torun = builtins[name];

        try
        {
            vm->traceback.push_back(val);
            torun(val, vm);

            vm->traceback.pop_back();
        }
        catch (const std::exception &e)
        {
            Utils::error(std::string("an error occured whilst executing a builtin. Error: ") + e.what(), vm);
        }
    }
    // Finally, if the string matched nothing above, it's a normal string, so push it.
    else
    {
        vm->stack.push_back(token);
    }
}


// Interprets tokens and pushes their results to the stack provided
void interpret(Substack code, BambooVM* vm)
{
    Token prev{0};

    for (auto token = code.begin(); token != code.end(); ++token )
    {
        int stackLength = vm->stack.size();


        // If the token is an int, substack, or raw string. push it without question.
        if ( HOLDS<int>(token->_data) || HOLDS<Substack>(token->_data) || token->isRawString() )
        { vm->stack.push_back(*token); }
        // if the token is a string, there are some more things to do with it before pushing
        else
        {
            try
            {
                if (std::get<std::string>(token->_data) == "if")
                {
start:              BambooVM temp = vm->makeChild();

                    // progress token and interpret it
                    token++;

                    // progressing it brings it to the token past the IF
                    // which should be the condition

                    if (HOLDS<std::string>(token->_data))
                    {

                        auto s = std::get<std::string>(token->_data);
                        // check if its a named value, if not then the string cant be used as a condition
                        if (s.at(0) == '.')
                        {
                            if (vm->namedValues.find(s.substr(1, s.size())) == vm->namedValues.end())
                                Utils::error("named value `" + s.substr(1, s.size()) + "` does not exist", vm, *token);
                            interpret(*vm->namedValues[s.substr(1, s.size())], &temp);
                        }
                        else
                            Utils::error("A string is not able to be interpreted as a true or false value. Use a substack or integer instead.", vm, *token);

                    }
                    else if (HOLDS<Substack>(token->_data))
                        interpret(std::get<Substack>(token->_data), &temp);
                    else
                        interpret(*token, &temp);

                    // progress token to the code stack
                    token++;

                    // ensure the last value on the condition stack is a integer
                    if (!HOLDS<int>(temp.stack.back()._data))
                    {
                        std:: cout << temp.stack.back().getContent() << std::endl;
                        Utils::coutStackWithColors(temp.stack);
                        std::cout << std::endl; // move to next line bc the cout stack func doesnt add a \n lol
                        Utils::error("Last value on the condition stack is not a integer. Condition stack has been printed above ^^^^");
                    }

                    // if the last value pushed to the condition stack is true,
                    // interpret the code stack
                    if (std::get<int>(temp.stack.back()._data))
                    {
                        if (HOLDS<Substack>(token->_data))
                            interpret(std::get<Substack>(token->_data), vm);
                        else
                            interpret(*token, vm);

                        while( token++, !(token==code.end()) )
                            if (HOLDS<std::string>(token->_data) && std::get<std::string>(token->_data) == "end")
                                break;

                        continue;

                    }
                    else
                    {
                        // check if there is an else after the code stack
                        // if there isnt then we dont need to do anything else
                        if (++token == code.end() || !HOLDS<std::string>(token->_data))
                        { continue; }


                        if (std::get<std::string>(token->_data) == "else")
                        {
                            // since its an else, progress to the next token
                            // to see if it can be interpreted
                            token++;
                            if (HOLDS<Substack>(token->_data))
                                interpret(std::get<Substack>(token->_data), vm);
                            else
                                if (HOLDS<std::string>(token->_data) && std::get<std::string>(token->_data) == "if")
                                    goto start;
                                else
                                    interpret(*token, vm);
                            // go past the "end" token.
                            token++;
                            continue;
                        }
                    }

                }
                else if (std::get<std::string>(token->_data) == "while")
                {
                    BambooVM temp = vm->makeChild();

                    Token condition = *(++token);
                    Token code = *(++token);

                    if (HOLDS<Substack>(condition._data))
                        interpret(std::get<Substack>(condition._data), &temp);
                    else if (HOLDS<int>(condition._data))
                        interpret(condition, &temp);
                    else
                        Utils::error("A string is not able to be interpreted as a true or false value. Use a substack or integer instead.", vm, condition);


                    while(std::get<int>(temp.stack.back()._data))
                    {
                        temp.stack.clear();
                        interpret(std::get<Substack>(code._data), vm);

                        if (HOLDS<Substack>(condition._data))
                            interpret(std::get<Substack>(condition._data), &temp);
                        else
                            interpret(condition, &temp);
                    }

                    return;

                }
                pushValueOfToken(*token, vm);
            }
            catch (const int &errorCode)
            {
                // #return or #exit function was called
                if (errorCode == 0)
                    return;
            }
        }

        prev = *token;
    }
}


// Interprets a token and pushes it's result to the stack provided
void interpret(Token token, BambooVM* vm)
{
    int stackLength = vm->stack.size();

    // If the token is an int, substack, or raw string. push it without question.
    if ( HOLDS<int>(token._data) || HOLDS<Substack>(token._data) || token.isRawString())
    { vm->stack.push_back(token); }
    // if the token is a string, there are some more things to do with it before pushing
    else
    { pushValueOfToken(token, vm); }
}

#endif