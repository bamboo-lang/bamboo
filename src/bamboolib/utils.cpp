#include <iostream>
#include <fstream>
#include <string>

#include "types.cpp"
#include "utils.hpp"

#ifndef __bamboolib_utils_cpp__
#define __bamboolib_utils_cpp__

std::vector<std::string> Utils::split(std::string s)
{
    std::string buffer = "";

    std::vector<std::string> strings;

    bool hitString = false;
    char last;

    for (char c : s)
    {
        if (c == '\t') continue;

        // hit start of a comment
        if (c == ';')
        {
            if (buffer != "") strings.push_back(buffer);
            return strings;
        }

        if (last == '\\' && c == '\\')
        {
            buffer += '\\';
            last = c;
            continue;
        }
        else if (last == '\\')
        {
            buffer.pop_back();
            switch (c)
            {
                case 'n': buffer += '\n'; break;
                case 't': buffer += '\t'; break;
                case 'r': buffer += '\r'; break;
                case 'f': buffer += '\f'; break;
                default: buffer += c;
            }
            last = c;
            continue;
        }

        if (c == '"' && last != '\\')
        {
            // toggle value of hit string to be the oppisite
            // ex hitString = true, it will now be false.
            hitString = !hitString;
            // dont want to push this char to the buffer
        }

        // not in string
        if (hitString == false)
        {
            // if its just a space, push back the buffer
            if (c == ' ')
            {
                if (buffer.empty() == false)
                {
                    strings.push_back(buffer);
                    buffer.clear();
                }
                continue;
            }
            // if its a sub stack token, push back the buffer and then the token
            // both seperatly
            else if (c == '{' || c == '}' || c == '(' || c == ')')
            {
                if (buffer.empty() == false)
                {
                    strings.push_back(buffer);
                    buffer.clear();
                }

                strings.push_back(std::string(1, c));
                continue;

            }
        }

        // will only be reached if not on a space, quote, or open/close brackets.
        buffer += c;
        last = c;
    }

    // if there is a word at very end of string
    // push it back
    if (buffer.empty() == false)
    {
        strings.push_back(buffer);
    }

    return strings;
}

// Util to print a stack
void Utils::coutStack(Substack stack)
{
    std::cout << "{ ";

    for (Token t : stack)
    {

        if (HOLDS<std::string>(t._data))
            std::cout << (t.rawString ? "\"" + std::get<std::string>(t._data) + "\"" : std::get<std::string>(t._data)) << ", ";

        else if (HOLDS< Substack >(t._data))
            coutStack( std::get< Substack >(t._data) );

        else
            std::cout << std::get<int>(t._data) << ", ";
    }

    std::cout << "} ";
}

void Utils::coutStackWithColors(Substack stack)
{
    std::cout << "{ ";

    for (Token &t : stack)
    {

        if (HOLDS<std::string>(t._data))
            std::cout << "\033[33m" << (t.rawString ? "\"" + std::get<std::string>(t._data) + "\"" : std::get<std::string>(t._data)) << "\033[0m, ";

        else if (HOLDS< Substack >(t._data))
            coutStackWithColors( std::get< Substack >(t._data) );

        else
            std::cout << "\033[35m" << std::get<int>(t._data) << "\033[0m, ";
    }

    std::cout << "} ";
}

void Utils::error(std::string message, BambooVM* vm, Token token)
{
    std::cout << "\033[31mError: \033[0m" << message << std::endl;
    if (vm)
    {
        std::cout << "\033[31mError: \033[0m" << "Token: " << (token.isRawString() ? "'" + token.getContent() + "'" : token.getContent()) << " | at -> " << token.location << std::endl;
        std::cout << "\033[31mContext: \033[0m" << std::endl;
        LimitedList<std::string> context = LimitedList<std::string>(3);

        /****************************
        * START OF CONTEXT BUILDER *
        ****************************/
        if (vm->fileVMBelongsTo != "none")
        {
            std::fstream locationOfTokens(vm->fileVMBelongsTo);

            int lineN = 0;
            std::string line;
            // grab the previous two lines before the error line
            // and then grab the error line
            while (std::getline(locationOfTokens, line))
            {
                lineN++;

                if (lineN > token.location)
                    break;

                if (lineN >= token.location - 3)
                    context.push(line);
            }

            lineN = token.location - context._internal.size() + 1;

            auto itr = context._internal.begin();

            // if the error token line is divisible by 10, then the previous
            // two line numbers are one digit less in size.
            // we need to print those lines with a space infront of the
            // line number so that everything is aligned.
            if (token.location % 10 == 0)
            {
                std::cout << "\033[34m " + std::to_string(lineN++) << "| \033[0m" << *itr++ << std::endl;
                std::cout << "\033[34m " + std::to_string(lineN++) << "| \033[0m" << *itr++ << std::endl;
                std::cout << "\033[34m" + std::to_string(lineN) << "| \033[0m" << *itr << std::endl;
            }
            else // If not, then everything can be printed on the same line
            {
                switch (token.location)
                {
                    case 1:
                        std::cout << "\033[34m" + std::to_string(lineN) + "| \033[0m" << *itr << std::endl;
                        break;
                    case 2:
                        std::cout << "\033[34m" + std::to_string(lineN++) << "| \033[0m" << *itr++ << std::endl;
                        std::cout << "\033[34m" + std::to_string(lineN) << "| \033[0m" << *itr << std::endl;
                        break;
                    case 3:
                        std::cout << "\033[34m" + std::to_string(lineN++) << "| \033[0m" << *itr++ << std::endl;
                        std::cout << "\033[34m" + std::to_string(lineN++) << "| \033[0m" << *itr++ << std::endl;
                        std::cout << "\033[34m" + std::to_string(lineN) << "| \033[0m" << *itr << std::endl;
                        break;
                    default:
                        break;
                };
            }

            // print each line in context

            std::cout  << "\033[31m";

            // print spaces up until we hit the start of
            // the token that caused an error
            std::string toFind;
            int underlineDistance;
            if (token.getContent() == "#:")
            {
                toFind              = "(";
                int start           = (int)context._internal.back().find("(");
                int end             = (int)context._internal.back().find(")") + 1;
                underlineDistance   = end - start;
            }
            else
            {
                // store result of std::get() inside of a variable
                // value to avoid calling it again
                std::string s = token.getContent();

                toFind              = s;
                underlineDistance   = s.size();
            }
// Print spaces from the length of the line number, + 2 ( the `|` and ` ` immediatly after ), and then + the position of the error token
            for (int i = 0; i < context._internal.back().find(toFind) + std::to_string(token.location).size() + 2; i++)
                std::cout << " ";


            // print fancy underline under the error token
            for (int i = 0; i < underlineDistance; i++)
                std::cout << "~";

            std::cout << "\033[0m" << std::endl;

        }
        else
        {
            std::cout << "No file found for error within current VM" << std::endl;
        }
        /**************************
        * END OF CONTEXT BUILDER *
        **************************/

        std::string tb;
        for (const std::string &s : vm->traceback) tb.append(s + "\033[31m > \033[0m");
        std::cout << "\033[31mTraceback: \033[0m" << tb << std::endl;

        std::cout << "\033[31mStack:\033[0m" << std::endl;
        coutStackWithColors(vm->stack);
        std::cout << std::endl;
    }

    exit(1);
}

void Utils::warn(std::string message)
{
    std::cout << "\033[34mwarning: \033[0m" << message << std::endl;
}

void Utils::strReplace(std::string &str, std::string substr, std::string replaceWith)
{
    if (str.find(substr) == std::string::npos) return;

    int size = replaceWith.size() + 1;
    std::string::size_type index = 0;
    while ((index = str.find(substr, index)) != std::string::npos) {
        str.replace(index, size, replaceWith);
        ++index;
    }
}

#endif