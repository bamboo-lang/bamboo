#include "types.hpp"

#ifndef __bamboolib_interpreter_hpp__
#define __bamboolib_interpreter_hpp__

inline void pushValueOfToken(Token &token, BambooVM* vm);

// Interprets tokens and pushes their results to the stack provided
void interpret(Substack code, BambooVM* vm);

void interpret(Token code, BambooVM* vm);

#endif