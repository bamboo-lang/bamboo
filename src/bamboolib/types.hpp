#include <string>
#include <functional>
#include <list>
#include <variant>

#ifndef __bamboolib_types_hpp__
#define __bamboolib_types_hpp__

#define TOKEN_TYPE_STR 0
#define TOKEN_TYPE_INT 1
#define TOKEN_TYPE_SUB 2

// used to avoid typing the entire function name
#define HOLDS std::holds_alternative

class Token;

struct BambooVM
{
    // name of the file the VM was created for
    // used for debug
    std::string fileVMBelongsTo = "none";
    std::list<Token> stack;
    std::unordered_map<std::string, Token*> namedValues;
    std::string libdir = "~/.bamboo/libs/";
    std::vector<std::string> traceback;
    std::vector<std::string> argv;
    std::list<std::string> imports;
    std::list<std::string> injects;
    int argc;
    BambooVM* callee = this;

    inline BambooVM copy()
    {
        return BambooVM{
            this->fileVMBelongsTo,
            std::list<Token>(),
            this->namedValues,
            this->libdir,
            this->traceback,
            this->argv,
            this->imports,
            this->injects,
            this->argc
        };
    }

    inline BambooVM makeChild()
    {
        return BambooVM{
            this->fileVMBelongsTo,
            std::list<Token>(),
            this->namedValues,
            this->libdir,
            this->traceback,
            this->argv,
            this->imports,
            this->injects,
            this->argc,
            this
        };
    }
};

struct RawToken {
    int line;
	std::string content;
};

typedef std::function<void(std::string, BambooVM* vm)> BuiltinFunction;
typedef std::list<Token> Substack;

// A token can store a string, int, or a substack
struct Token
{
    bool rawString;

    std::variant<int, std::string, std::list<Token>> _data;

    int location;

    Token(std::string val, bool rawString = false, int line = -1)
    :_data(val), rawString(rawString), location(line) {}

    Token(int val, int line = -1)
    :_data(val), location(line) {}

    Token(Substack val, int line = -1)
    :_data(val), location(line) {}

    // Creates a token from raw data represented as a string.
    // Cannot convert to substack.
    static Token fromRawData(std::string data, int line);

    // Returns the content of _data as a string.
    // If it is a substack, it will return "<substack>"
    std::string getContent();

    inline bool isRawString();

    inline void setRawString(bool value);

    // Returns the token type represented as an integer
    inline int getType();

    // Returns tokent type represented by a string
    inline std::string getTypeAsString();
};

// wrapper for std::list that limits the amount of elements
// that can be contained
// used only once in BambooVM
template <class T>
class LimitedList {

    public:
        std::list<T> _internal;
        int limit;

        LimitedList(int limit)
        : limit(limit) {}

        inline void push(T item);
        inline void pop();
};

#endif
