#include "types.hpp"

#ifndef __bamboolib_utils_hpp__
#define __bamboolib_utils_hpp__

class Utils {
    public:

        // splits a string by spaces and brackets
        static std::vector<std::string> split(std::string s);

        // replace a substring with another substring in a string
        static void strReplace(std::string &str, std::string substr, std::string replaceWith);

        // Util to print a stack
        static void coutStack(Substack vec);
        static void coutStackWithColors(Substack stack);

        // Display an error message and exit.
        static void error(std::string message, BambooVM* vm = 0, Token token = 0);

        // Display a warning message.
        static void warn(std::string message);
};

#endif
