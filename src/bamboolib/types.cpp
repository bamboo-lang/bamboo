#include <unordered_map>

#include "utils.hpp"
#include "types.hpp"

#ifndef __bamboolib_types_cpp__
#define __bamboolib_types_cpp__

inline Token Token::fromRawData(std::string data, int line)
{
    try // return token of right type
    {
        return Token(std::stoi(data), line);
    }
    catch (const std::exception& e)
    {
        // is raw string? ( inside of quotes )
        if (data.at(0) == '"')
        {
            // get a substring of the contents of the raw string
            // set the token.rawString flag as true.
            return Token( data.substr(1, data.size() - 2), true, line );
        }
        else
        {
            // if not, just push back the token
            return Token(data, false, line);
        }
    }
}

inline std::string Token::getContent()
{
    if (HOLDS<int>(_data))
        return std::to_string(std::get<int>(_data));

    if (HOLDS<std::string>(_data))
        return std::get<std::string>(_data);

    return "<substack>";
}

inline bool Token::isRawString()
{ return Token::rawString; }

inline void Token::setRawString(bool value)
{ Token::rawString = value; }

inline int Token::getType()
{
    if (HOLDS<int>(_data))
        return TOKEN_TYPE_INT;

    if (HOLDS<std::string>(_data))
        return TOKEN_TYPE_STR;

    return TOKEN_TYPE_SUB;
}

inline std::string Token::getTypeAsString()
{
    switch(getType())
    {
        case TOKEN_TYPE_INT: return "int";
        case TOKEN_TYPE_STR: return "string";
        default:             return "<substack>";
    };
}

template <typename T>
inline void LimitedList<T>::push(T item)
{
    if (LimitedList<T>::_internal.size() + 1 > limit)
    {
        LimitedList<T>::_internal.pop_front();
    }

    LimitedList<T>::_internal.push_back(item);
}

template <typename T>
inline void LimitedList<T>::pop()
{
    LimitedList<T>::_internal.pop_back();
}

#endif