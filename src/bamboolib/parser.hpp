#include "types.cpp"
#include <fstream>

#ifndef __bamboolib_parser_hpp__
#define __bamboolib_parser_hpp__

inline Token parseNextToken(std::fstream &file);

// parse a file
void parse(std::string fileName, BambooVM &fileVM);

#endif