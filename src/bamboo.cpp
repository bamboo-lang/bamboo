#include <exception>
#include <fstream>
#include <pwd.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <sys/stat.h>

#include "bamboolib/parser.hpp"
#include "bamboolib/utils.hpp"

const std::string HELP =
	"\033[1musage\033[0m: bamboo action [file] [options]\n\
\033[1mactions\033[0m:\n\
	run	          run a provided file\n\
	create        create an empty bamboo project\n\
	help          display this message\n\
\n\
\033[1moptions\033[0m:\n\
	-s  --stack   shows the output stack after execution\n\
	-l  --libdir  change the libdir path\n\
	-q  --quiet   prevents debug info\n\
\n\
any arguments passed after `--` are passed as program arguments\n\
\n\
bamboo's license can be found here: https://gitlab.com/bamboo-lang/bamboo/-/blob/main/LICENSE";

const std::string TEMPLATE_BAMBOO_FILE = "\"Hello, World!\" #print #pop";

inline bool fileExists(const char* path)
{
	struct stat buffer;
	return (stat(path, &buffer) == 0);
}

inline void createProject(int argc, char** argv)
{
	std::string projectName = argv[2];

	if (fileExists("main.bam"))
		Utils::error("Bamboo: create: main.bam already exists.");
	if (fileExists("README.md"))
		Utils::error("Bamboo: create: README.md already exists.");

	std::ofstream mainbam("main.bam");
	mainbam << TEMPLATE_BAMBOO_FILE << std::endl;
	mainbam.close();

	std::ofstream readmemd("README.md");
	readmemd << "# " << projectName << std::endl;
	readmemd.close();
}

inline void displayHelp()
{
	std::cout << HELP << std::endl;
}

inline void displayStack(BambooVM &vm)
{
	std::cout << "\033[34mOutput stack (from -s):\033[0m" << std::endl;
	Utils::coutStackWithColors(vm.stack);
	std::cout << std::endl;
}

int main(int argc, char** argv)
{
	// check to make sure an action is provided
	if (argc == 1)
		Utils::error("Bamboo: no action provided. See `bamboo help`");

	bool coutStack = false, passingArgsToProgram = false, quiet = false;
	std::vector<std::string> programArgs;
	std::string
		fileToRun,
		libdir = std::string(getpwuid(getuid())->pw_dir) + "/.bamboo/libs/", // this is $HOME/.bamboo/libs/
		action = argv[1];

	if (action == "run") // run a file
		fileToRun = argv[2];
	else if (action == "create") // create a new project
	{
		createProject(argc, argv);
		return 0;
	}
	else if (action == "help") // display help
	{
		displayHelp();
		return 0;
	}
	else
		Utils::error("unknown action `" + action + "`");

	// if there are still more args, start parsing them!
	if (argc > 3)
	{
		for (int i = 3; i < argc; i++)
		{
			std::string arg(argv[i]);

			if (arg == "-s" || arg == "--stack")
				coutStack = true;
			else if (arg == "-l" || arg == "--libdir")
			{
				libdir = argv[i + 1];
				i++; // skip the next arg
			}
			else if (arg == "-q" || arg == "--quiet")
				quiet = true;
			else if (arg == "--") // start passing args to the program rather than bamboo
				passingArgsToProgram = true;
			else if (passingArgsToProgram)
				programArgs.push_back(arg);
			else
				Utils::warn("Bamboo: unknown argument, skipping: " + arg);
		}
	}

	if (!quiet)
	{
		std::cout << "\033[34mBamboo\033[0m: libdir `" << libdir << '`' << std::endl;
		std::cout << "\033[34mBamboo\033[0m: running `" << fileToRun << '`' << std::endl;
	}

    if (!fileExists(fileToRun.c_str()))
        Utils::error("Bamboo: no such file" + fileToRun);

	// create the VM
	BambooVM vm;
	vm.fileVMBelongsTo = fileToRun;
	vm.argc = programArgs.size();
	vm.argv = programArgs;
	vm.libdir = libdir;
	vm.namedValues["_FILE"] = new Token(fileToRun);
	vm.namedValues["_LIBDIR"] = new Token(libdir);

	// run the file, if there is an error, `cout` that error
	try
	{ parse(fileToRun, vm); }
	catch (const std::exception &e)
	{ std::cout << "\033[31mError:\033[0m " << e.what() << std::endl; }

	// cout the stack
	if (coutStack) displayStack(vm);
}
