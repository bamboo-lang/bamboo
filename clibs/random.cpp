#include "../src/bamboolib/types.cpp"
#include "../src/bamboolib/utils.hpp"
#include <iostream>

extern "C" void inject(BambooVM* vm, std::unordered_map<std::string, BuiltinFunction>* builtins)
{
    builtins->insert({"rand", [] (std::string val, BambooVM* vm) {
        vm->stack.push_back(std::rand());
    }});

    builtins->insert({"seed", [] (std::string val, BambooVM* vm) {
        if (HOLDS<int>(vm->stack.back()._data))
            std::srand( (unsigned) std::get<int>(vm->stack.back()._data) );
        else
            Utils::error(
                "#seed requires token of type int for argument. Token `"
                + vm->stack.back().getContent() +
                "` is of token type "
                 + std::to_string(vm->stack.back().getType())
            );
    }});

    builtins->insert({"get-time", [] (std::string val, BambooVM* vm) {
        vm->stack.push_back( Token( (int)time(NULL) ) );
    }});
};