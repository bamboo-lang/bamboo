#include <iostream>
#include <unordered_map>
#include "../src/bamboolib/types.cpp"

extern "C" void inject(BambooVM* vm, std::unordered_map<std::string, BuiltinFunction>* builtins)
{
    builtins->insert({ "system", [] (std::string val, BambooVM* vm) {
        if (!HOLDS<std::string>(vm->stack.back()._data))
            Utils::error(
                "#system requires token of type int for argument. Token `"
                + vm->stack.back().getContent() +
                "` is of type "
                + vm->stack.back().getTypeAsString()
            );

        std::string cmd = std::get<std::string>(vm->stack.back()._data);
        vm->stack.pop_back();
        std::system(cmd.c_str());
    }});
}
