#include <unordered_map>
#include <variant>
#include "../src/bamboolib/types.cpp"
#include "../src/bamboolib/interpreter.hpp"

std::vector<std::string> elementStack;

extern "C" void inject(BambooVM* vm, std::unordered_map<std::string, BuiltinFunction>* builtins)
{
    builtins->insert({ "e", [] (std::string val, BambooVM* vm) {

		std::string element;
        try
        { element = std::get<std::string>(vm->stack.back()._data); }
        catch (const std::bad_variant_access &e)
        {
            Utils::error(
                "#e requires token of type string for argument. Token `"
                + vm->stack.back().getContent()
                + "` is of type " +
                vm->stack.back().getTypeAsString()
            );
        }

        elementStack.push_back(element);
        vm->stack.pop_back();
        vm->stack.push_back(Token("<" + element + ">"));

    }});

	builtins->insert({ "end", [] (std::string val, BambooVM* vm) {
        vm->stack.push_back(Token("</" + elementStack.back() + ">"));
		elementStack.pop_back();
    }});

    builtins->insert({ "build", [] (std::string val, BambooVM* vm) {
        if (!HOLDS<Substack>(vm->stack.back()._data))
            Utils::error(
                "#build requires token of type <substack> for argument. Token `"
                + vm->stack.back().getContent() +
                "` is of type "
                + vm->stack.back().getTypeAsString()
            );

        BambooVM tempVm = vm->copy();
        interpret(std::get<Substack>(vm->stack.back()._data), &tempVm);
        std::string html = "<!DOCTYPE html>";

        for (auto t : tempVm.stack)
            html += std::get<std::string>(t._data);

        vm->stack.push_back(Token(html));
    }});
}
