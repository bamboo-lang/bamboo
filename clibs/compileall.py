import os

print("Compiling libraries")
for l in [a for a in os.listdir(".") if a.endswith(".cpp")]:
	print(f"> Compiling {l}")
	os.system(f"g++ -fPIC {l} -shared -o ./{l.replace('.cpp', '.so')}")
print("Done compiling libraries")
