#include <fstream>
#include <unordered_map>
#include <variant>
#include "../src/bamboolib/types.cpp"

std::vector<std::string> elementStack;

extern "C" void inject(BambooVM* vm, std::unordered_map<std::string, BuiltinFunction>* builtins)
{
    builtins->insert({ "fileio-write", [] (std::string val, BambooVM* vm) {
		auto itr = vm->stack.end();
		std::string filePath;
		std::string toWrite;
		try
		{
			std::string filePath 	= std::get<std::string>( (--itr) ->_data);
			std::string toWrite 	= std::get<std::string>( (--itr) ->_data);
		}
		catch(const std::bad_variant_access &e)
		{
			Utils::error("#fileio-write requires two arguments of type string before function call. ex: `filePath toWrite #fileio-write`", vm);
		}

		std::ofstream file;
		file.open(filePath);

		try
		{
			file << toWrite;
		}
		catch (const std::exception &e)
		{
			file.close();
			throw e;
		}

		file.close();
    }});
}
